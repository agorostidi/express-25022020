require('dotenv').config()
require('./config/db')

const createError = require('http-errors')
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')

const passport = require('passport')
require('./config/passport')

const indexRouter = require('./routes/index.routes')
const authRouter = require('./routes/auth.routes')

const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(passport.initialize())

app.use('/', indexRouter)
app.use('/auth', authRouter)

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404))
})

// error handler
app.use((err, req, res, next) => {
  // render the error page
  res.status(err.status || 500).json(err.message)
})

module.exports = app
